# https://hub.docker.com/_/golang/tags
# Debian 12 with go 1.24.1
FROM golang:1.24.1

# Disable go telemetry
RUN go telemetry off

# Install docker
COPY --from=docker:latest /usr/local/bin/docker /usr/local/bin/docker

# Install docker buildx
COPY --from=docker/buildx-bin:latest /buildx /usr/libexec/docker/cli-plugins/docker-buildx

# Install some commonly used packages.
RUN apt-get -qqy update && apt-get install -qqy \
    curl \
    apt-transport-https \
    lsb-release \
    openssh-client \
    git \
    make \
    gnupg \
    zip

# Install kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl