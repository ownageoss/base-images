# Base Images

A set of opinionated base images.

Since this repo hosts multiple images, images will be tagged with appropriate names.

## Dockerfile.golang

This image will include the latest stable versions of:

- [Go](https://go.dev/dl/)
- [golangci-lint](https://github.com/golangci/golangci-lint)
- [gosec](https://github.com/securego/gosec)
- [govulncheck](golang.org/x/vuln/cmd/govulncheck)
- [gitleaks](https://github.com/gitleaks/gitleaks)

Mostly used for linting and security analysis of Go code.

Tags:

```sh
registry.gitlab.com/ownageoss/base-images:golang-latest
registry.gitlab.com/ownageoss/base-images:go<VERSION>
```

## Dockerfile.gcloud

This image will include the latest stable versions of:

- [Google Cloud SDK](https://hub.docker.com/r/google/cloud-sdk)
- [Go](https://go.dev/dl/)

Mostly used for building containers, using the go command, creating k8s manifests and deploying to k8s.

Note: This is not limited to Google cloud in any way.

Tag:

```sh
registry.gitlab.com/ownageoss/base-images:gcloud-latest
```

## Dockerfile.aws

This image will include the stable versions of:

- [Google Cloud SDK](https://hub.docker.com/r/google/cloud-sdk)
- [Go](https://go.dev/dl/)
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [eksctl](https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html)

Mostly used for building containers, using the go command, creating k8s manifests and deploying to k8s on AWS.

Tag:

```sh
registry.gitlab.com/ownageoss/base-images:aws-latest
```

## Dockerfile.devcontainer

An image for use as a dev container in VS code:

- Debian 12
- Go
- Rust
- kubectl
- helm
- nodejs
- psql

```sh
# Example
registry.gitlab.com/ownageoss/base-images:devcontainer-latest
registry.gitlab.com/ownageoss/base-images:devcontainer-24.4.1 (yy.mm.count)
```

Note: [Dev container features](https://github.com/devcontainers/features) have their own release schedule and the main intention here is to operate at a faster velocity. Hence, some features are pre-installed in the base image.

## Updates

Base images will be updated when there is a new version of Go, Rust or when packages need to be updated.